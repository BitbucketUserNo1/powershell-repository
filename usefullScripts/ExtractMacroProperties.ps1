$StandardName = "CS analog I"
$xmlPath = "Y:\makro\$StandardName.emp"
$xlsxPath ="Y:\makro\$StandardName.xlsx"

$table = @()
$logFile = "log.txt"
$errorCode = 0


Import-Module PSExcel  
# Load assembly
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

try {
	try {
		$xml = New-Object -TypeName XML
		$xml.Load($xmlPath)
	}
	catch {
		"$(Get-Date )`tXML import failed`t" + $Error| Add-Content $logFile
		$ErrorCode = 2
	}
	try {
		$properties = $xml.EplanPxfRoot.O4.O106.A1896.Split(";", [System.StringSplitOptions]::RemoveEmptyEntries)
		$properties += "Urządzenie"
	}
	catch {
		"$(Get-Date )`tPlacehoder in XLM not found`t" + $Error| Add-Content $logFile
		$ErrorCode = 3
	}
	try {
		$myObject = New-Object -TypeName PSCustomObject
		ForEach ( $noteProperty in $properties) {$myObject | Add-Member -MemberType NoteProperty -Name $noteProperty -Value ''} 
		$table += $myObject
		 $table | Export-XLSX -Path $xlsxPath -Force
	}
	catch {
		"$(Get-Date )`tExcel write error`t" + $Error| Add-Content $logFile
		$ErrorCode = 4
	}
}
catch {
    "$(Get-Date )`tScript excetution failed`t" + $Error| Add-Content $logFile
    $ErrorCode = 1
}
If ($errorCode) {[System.Windows.Forms.MessageBox]::Show("Excetution failed with error $($errorCode). Details in $($logFile)","Excetution failed",[System.Windows.Forms.MessageBoxButtons]::OK,[System.Windows.Forms.MessageBoxIcon]::Warning)}
