Import-Module PSExcel
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

$StandardName = "CS analog O"
$xmlPath = "Y:\makro\$StandardName.emp"
$xlsxPath ="Y:\makro\$StandardName.xlsx"
$xmlPathNew="C:\Users\walpuskik\Desktop\$StandardName.emp"
$logFile = "log.txt"
$errorCode = 0


try {
	try {
		$xml = New-Object -TypeName XML
		$xml.Load($xmlPath)
	}
	catch {
		"$(Get-Date )`tXML import failed`t" + $Error| Add-Content $logFile
		$ErrorCode = 2
	}
	try {
		$placeHolderNames = $xml.EplanPxfRoot.O4.O106.A1896.Split(";", [System.StringSplitOptions]::RemoveEmptyEntries)
        $placeHolderValues = $xml.EplanPxfRoot.O4.O106.S114x1894.A2012
        
	}
	catch {
		"$(Get-Date )`tPlacehoder in XLM not found`t" + $Error| Add-Content $logFile
		$ErrorCode = 3
	}
	try {
		$table = Import-XLSX -Path $xlsxPath
	}
	catch {
		"$(Get-Date )`tExcel import error`t" + $Error| Add-Content $logFile
		$ErrorCode = 4
	}
	try {
		$SourceName = 'O4'
        $TempName = 'TEMP'

        $ParentNode = $xml.EplanPxfRoot
        $CloneNode = $ParentNode.SelectSingleNode($SourceName)
   
        $TempNode = $xml.CreateElement($TempName)
        $TempNode.InnerXML = $CloneNode.InnerXML
   
        [void]$ParentNode.AppendChild($TempNode)
        [void]$ParentNode.RemoveChild($CloneNode)

		$i=0
		ForEach ($row in $table) {
			$i++
			$kks = $row.Urz�dzenie
			Write-Progress -Activity (�Dodawanie strony (� + $i + "/" + $table.count +")")  -status �Kopiuj� "+ $kks -percentComplete ($i / $table.count*100)

            $TempNode.P49.SetAttribute("P1100", $kks)
            $TempNode.O106.S114x1894.A2011 = $kks
            $TempNode.O106.S114x1894.A2012 = $placeHolderValues

            ForEach ($Name in $placeHolderNames){
                $TempNode.O106.S114x1894.A2012 = $TempNode.O106.S114x1894.A2012 -replace $Name, $row.$Name
                }
            $CloneNode = $ParentNode.SelectSingleNode($TempName)
            $NewNode = $xml.CreateElement($SourceName)

            $NewNode.InnerXML = $CloneNode.InnerXML
            [void]$ParentNode.AppendChild($NewNode)
        }
        [void]$ParentNode.RemoveChild($TempNode)
		$xml.Save($xmlPathNew)
		
	}
	catch {
		"$(Get-Date )`tEMP modifiing error`t" + $Error| Add-Content $logFile
		$ErrorCode = 5
	}
}
catch {
    "$(Get-Date )`tScript excetution failed`t" + $Error| Add-Content $logFile
    $ErrorCode = 1
}
If ($errorCode) {[System.Windows.Forms.MessageBox]::Show("Excetution failed with error $($errorCode). Details in $($logFile)","Excetution failed",[System.Windows.Forms.MessageBoxButtons]::OK,[System.Windows.Forms.MessageBoxIcon]::Warning)}
