$path = "W:\Udostepnione\Zadania\2014\2014DA053 ZTUO Szczecin\04_Zalozenia\01_Baza\01_Baza Danych"
$diff_app = New-Object -ComObject Excel.Application
$diff_app.visible = $true
$diff_app.DisplayAlerts = $false
$pf="W:\Udostepnione\Zadania\2014\2014DA053 ZTUO Szczecin\09_Roboczy\Porównanie.xlsx"
$row=2

$ActiveWorkbook = $diff_app.WorkBooks.Open($pf)
$ActiveWorksheet = $ActiveWorkbook.Worksheets.Item(1)


Get-ChildItem -Path $path -Filter *.xls? | Sort-Object -property LastWriteTime -descending  | ForEach-Object {
    $ActiveWorksheet.Cells.Item($row,2) = $_.FullName
    $row++
}

#$diff_app.Quit()
[System.Runtime.Interopservices.Marshal]::ReleaseComObject($diff_app)
Remove-Variable diff_app