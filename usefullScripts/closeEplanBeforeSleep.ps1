#
# Script closing EPLAN begore sleep
#Based on: https://learn-powershell.net/2013/08/14/powershell-and-events-permanent-wmi-event-subscriptions/ and https://blogs.technet.microsoft.com/heyscriptingguy/2011/08/16/monitor-and-respond-to-windows-power-events-with-powershell/


#Creating a new event filter
#$instanceFilter = ([wmiclass]"\\.\root\subscription:__EventFilter").CreateInstance()
$Filter = New-WmiEventFilter -Name SystemSuspend -Query �select * from Win32_PowerManagementEvent where EventType = 4�
$Consumer = New-WmiEventConsumer -Verbose -Name SystemSuspendCloseEplan -ConsumerType CommandLine -CommandLineTemplate �powershell.exe -command 'Get-Process EPLAN |   Foreach-Object { $_.CloseMainWindow() | Out-Null }'�
New-WmiFilterToConsumerBinding -Filter $Filter -Consumer $Consumer