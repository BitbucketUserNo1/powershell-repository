$logFile = "log.txt"
$errorCode = 0
# Load assembly
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

try {
    $visio_app = New-Object -ComObject Visio.Application
    $visio_app.visible = $false
    echo "Pracuj�. Prosz� czeka�."


    Get-ChildItem  -Filter *.vsd? -Recurse | ForEach-Object {
        try {
	        $document = $visio_app.Documents.Open($_.FullName)
            $dwg_filename = "$($_.DirectoryName)\$($_.BaseName).dwg"
            $visio_app.Addons.Item("Eksportowanie do programu AutoCAD").Run($dwg_filename)
    
            $wshell = New-Object -ComObject wscript.shell;
            $visio_app.AlertResponse = 2
            $document.Close()
    
	        echo "Zapisano $($dwg_filename)"
        }
        Catch {
            "$(Get-Date )`tNie uda�o si� zapisa� $($dwg_filename)`t" + $Error| Add-Content $logFile
            $ErrorCode = 1
        }
    }

    $visio_app.Quit()
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($visio_app)
    Remove-Variable visio_app
}
catch {
    "$(Get-Date )`tNie uda�o si� zamkn�� Visio $($dwg_filename)`t" + $Error| Add-Content $logFile
    $ErrorCode = 2
}
If ($errorCode) {[System.Windows.Forms.MessageBox]::Show("Co� posz�o nie tak. Szczeg�y w log.txt","B��d skryptu",[System.Windows.Forms.MessageBoxButtons]::OK,[System.Windows.Forms.MessageBoxIcon]::Warning)}
