$test = $true
$folderWatched = if ($test) {'D:\test\'} else {'X:\Wlasne\'}
$folderOldCopy = if ($test) {'D:\test\proba'} else {'X:\Wlasne\Robocze'}
$filter = '*.xls'
$scriptLogPath= 'D:\skrypty\logs'
$scriptName = $MyInvocation.MyCommand.Name
$fileName = "DE1 Auta.xls"

                         
$fsw = New-Object IO.FileSystemWatcher $folderWatched, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'LastWrite'} 
Register-ObjectEvent $fsw Changed -SourceIdentifier FileChanged -Action $actionBlock

$actionBlock = 
{
try
    {
    $fsw.EnableRaisingEvents = $false
    }
finally
    {
    $name = $Event.SourceEventArgs.Name
    $changeType = $Event.SourceEventArgs.ChangeType
    $timeStamp = $Event.TimeGenerated.GetDateTimeFormats("s")
    $logInfo = "$timeStamp - The file '$name' was $changeType"
    Write-Host $logInfo
    if ($name -eq $fileName)
        {
        Out-File -FilePath "$scriptLogPath\$scriptName.log" -Append -InputObject $logInfo
        #Import-Module PSExcel
        Get-CellValue $folderWatched

        }
    $fsw.EnableRaisingEvents = $true
    }
}
  

# Unregister-Event FileChanged