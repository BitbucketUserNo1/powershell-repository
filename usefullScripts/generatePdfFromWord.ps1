$word_app = New-Object -ComObject Word.Application
$word_app.visible = $false
echo "Pracuj�. Prosz� czeka�."

Get-ChildItem  -Filter *.doc? -Recurse | ForEach-Object {
	try {      
        $document = $word_app.Documents.Open($_.FullName, $false, $true)
        if(!(Test-Path -Path "$($_.DirectoryName)\pdf")){
            new-item -itemtype directory -path "$($_.DirectoryName)\pdf"
         }
 
        $pdf_filename = "$($_.DirectoryName)\pdf\$($_.BaseName).pdf"
        $document.SaveAs([ref] $pdf_filename, [ref] 17)
        $document.Close([ref] [Microsoft.Office.Interop.Word.WdSaveOptions]::wdDoNotSaveChanges)
	    echo "Zapisano $($_.DirectoryName)\pdf\$($_.BaseName).pdf"
    }
    catch
    {
        echo "Nie uda�o si� zapisa� $($_.DirectoryName)\pdf\$($_.BaseName).pdf"
    }
    
}

$word_app.Quit()
[System.Runtime.Interopservices.Marshal]::ReleaseComObject($word_app)
Remove-Variable word_app
Remove-Item generatorPDF.ps1